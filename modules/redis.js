const Redis = require("ioredis");
const path = require('path');
const redisData = require(path.join(__dirname, '..', 'secrets', 'redis.json'));
const redis = new Redis.Cluster (
    [
        { host: redisData['host'], port: redisData['port'] }
    ],
    {
        redisOptions: {
            password: redisData['password']
        }
    }
);

redis.on('error', function(err) {
    console.log(err);
});

// Create a shared Redis instance for the entire application.
// Redis is single-thread so you don't need to create multiple instances
// or use a connection pool.
module.exports = redis;