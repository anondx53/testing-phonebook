const Minio = require('minio');
const path = require('path');
const minioData = require(path.join(__dirname, '..', 'secrets', 'minio.json'));
const minio = new Minio.Client(minioData);

module.exports = minio;