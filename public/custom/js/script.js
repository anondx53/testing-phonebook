function changeIframe(loc) {
    if (loc == '/all') {
        document.getElementById('add').classList.remove('active');
        document.getElementById('dashboard').classList.add('active');
    }
    if (loc == '/add') {
        document.getElementById('add').classList.add('active');
        document.getElementById('dashboard').classList.remove('active');
    }
    document.getElementById('iframe').src = loc;
}