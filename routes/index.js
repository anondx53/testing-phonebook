var express = require('express');
const path = require('path');
const api = require('@opentelemetry/api')
const redis = require("../modules/redis");
const minio = require('../modules/minio');
const jaeger = require('../modules/jaeger');
const fs = require('fs');
var router = express.Router();
const multer = require("multer");
const upload = multer({ dest: path.join(__dirname, '..', 'public', 'uploads') });

jaeger('phone-book-testing-apps', 'development');

// Route web page.

router.get('/', function(req, res, next) {
  api.trace.getSpan(api.context.active());
  res.render('index', {src_uri: '/all'});
});

router.get('/all', async function(req, res, next) {
  api.trace.getSpan(api.context.active());
  const name = await redis.lrange('name', 0, -1);
  const phone = await redis.lrange('phone', 0, -1);
  const image = await redis.lrange('image', 0, -1);
  const data = {
    name: name,
    phone: phone,
    image: image,
    sum: name.length
  }

  for(var i=0; i < image.length; i++) {
    let imageName = image[i];

    minio.fGetObject('testing', `${image[i].split('.')[0]}.txt`, path.join(__dirname, '..', 'public', 'uploads', `${image[i].split('.')[0]}_minio.txt`), function(err){
      if(err) console.log(err);

      console.log('success get object');

      let imageData = fs.readFileSync(path.join(__dirname, '..', 'public', 'uploads', `${imageName.split('.')[0]}_minio.txt`), 'ascii');

      fs.writeFileSync(path.join(__dirname, '..', 'public', 'uploads', imageName), imageData, 'base64');
    });
  }

  res.render('all_data', { data });
});

router.get('/add', function(req, res, next) {
  api.trace.getSpan(api.context.active());
  res.render('add_data');
});

router.post('/add', upload.single('inputPicture'), async function(req, res, next) {
  api.trace.getSpan(api.context.active());
  const name = req.body.inputName;
  const phone = req.body.inputPhone;
  const image = fs.readFileSync(req.file.path, 'base64');
  const metadata = {
    'Content-Type': 'text/plain',
    'Origin-Mime-Type': req.file.mimetype
  }

  fs.writeFileSync(`${req.file.path}.txt`, image);

  minio.bucketExists('testing', function(err, exists) {
    if (err) console.log(err);

    if(!exists) {
      minio.makeBucket('testing', function(error) {
        if (error) console.log(error);

        console.log('Bucket created!');
      });
    }
  });

  minio.fPutObject('testing', `${req.file.filename}.txt`, `${req.file.path}.txt`, metadata, function(error, etag) {
    if (error) console.log(error);

    console.log('File Uploaded to Minio');
  });

  await redis.lpush('name', name);
  await redis.lpush('phone', phone);
  await redis.lpush('image', `${req.file.filename}.png`);

  res.redirect('/add');
});

module.exports = router;
